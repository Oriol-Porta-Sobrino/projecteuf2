package com.example.mp3;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Cancion extends AppCompatActivity {

    private MP3BBDD miMP3;
    private int rep = 2, pos;
    private MediaPlayer[] can;
    private boolean loaded = false;
    private List<BBDDCancion> canciones;
    private Button play, pause, stop, repetir;
    private ImageView imagen;
    private Cursor cursor;
    private TextView cancionNombre;
    int cont = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cancion);
        Bundle bundle = this.getIntent().getExtras();

        imagen = (ImageView) findViewById(R.id.imgPortada);
        play = (Button) findViewById(R.id.btnPlay);
        pause = (Button) findViewById(R.id.btnPause);
        stop = (Button) findViewById(R.id.btnStop);
        repetir = (Button) findViewById(R.id.btnRep);
        cancionNombre = (TextView) findViewById(R.id.txtCancion);
        cancionNombre.setText(bundle.getString("nombre").toString());
        pos = bundle.getInt("position");
        canciones = new ArrayList<BBDDCancion>();
        leerBase();
        can = new MediaPlayer[canciones.size()];
        llenarLista();
        repetir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                repetir(v);
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                play(v);
            }
        });
    }

    public void onPrepared(MediaPlayer player) {
        player.start();
    }

    public void play(View v) {
        cont++;
        if (cont == 2) {
            cont++;
        }
        if (can[pos] == null) {
            llenarLista();
            can[pos].setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    stop(v);
                }
            });
        }
        onPrepared(can[pos]);
    }

    public void pause(View v) {
        if (can[pos] != null) {
            can[pos].pause();
        }
    }

    public void stop(View v) {
        if (can[pos] != null) {
            can[pos].stop();
            llenarLista();
            //Toast.makeText(this, "Cancion parada", Toast.LENGTH_SHORT).show();
        }
    }

    public void repetir(View v) {
        if (rep == 1) {
            //cambiar aspecto
            Toast.makeText(this, "Repetir desactivado", Toast.LENGTH_SHORT).show();
            can[pos].setLooping(false);
            rep = 2;
        } else {
            //cambiar aspecto
            Toast.makeText(this, "Repetir activado", Toast.LENGTH_SHORT).show();
            can[pos].setLooping(true);
            rep = 1;
        }
    }

    public void next(View v) {
        if (pos < can.length -1) {
            if (can[pos].isPlaying()) {
                can[pos].stop();
                pos++;
                cancionNombre.setText(canciones.get(pos).getNombre().toString());
                can[pos].start();
            } else {
                pos++;
                cancionNombre.setText(canciones.get(pos).getNombre().toString());
            }
        } else {
            can[pos].stop();
            llenarLista();
            pos = 0;
            cancionNombre.setText(canciones.get(pos).getNombre().toString());
            can[pos].start();
        }
    }

    public void prev(View v) {
        if (pos >= 1) {
            if (can[pos].isPlaying()) {
                can[pos].stop();
                llenarLista();
                pos--;
                cancionNombre.setText(canciones.get(pos).getNombre().toString());
                can[pos].start();
            } else {
                pos--;
                cancionNombre.setText(canciones.get(pos).getNombre().toString());
            }
        } else {
            //Toast.makeText(this, "Ya no quedan canciones", Toast.LENGTH_SHORT).show();
            can[pos].stop();
            llenarLista();
            pos = can.length-1;
            cancionNombre.setText(canciones.get(pos).getNombre().toString());
            can[pos].start();
        }
    }

    public void leerBase() {
        Bundle bundle = this.getIntent().getExtras();
        miMP3 = new MP3BBDD(this, "ACDC", null, 1);
        SQLiteDatabase db = miMP3.getWritableDatabase();
        String[] args = new String[] {bundle.getInt("id")+""};
        BBDDCancion can;
        cursor = db.rawQuery("SELECT id, nombre, path FROM cancion WHERE album = ?", args);
        if (cursor!= null && cursor.moveToFirst())
        {
            do {
                int id = cursor.getInt(0);
                String nombre = cursor.getString(1);
                int ruta = cursor.getInt(2);
                can = new BBDDCancion(id, nombre, ruta);
                canciones.add(can);
            } while (cursor.moveToNext());

        }
        db.close();
    }

    public void llenarLista() {

        int i = 0;
        for (BBDDCancion canci : canciones) {
            can[i] = MediaPlayer.create(this, canci.getPath());
            i++;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        can[pos].stop();
    }
}
