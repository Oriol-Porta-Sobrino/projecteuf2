package com.example.mp3;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MP3BBDD extends SQLiteOpenHelper {

    String sqlCancion = "CREATE TABLE cancion (id integer primary key autoincrement, nombre text, path text, album integer not null, foreign key(album) references album(id));";

    String sqlAlbum = "CREATE TABLE album (id integer primary key autoincrement, nombre text, canciones integer);";

    public MP3BBDD(Context context, String name, SQLiteDatabase.CursorFactory factory,
                       int version) {
        super(context, name, factory, version);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(sqlCancion);
        db.execSQL(sqlAlbum);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS album");
        db.execSQL("DROP TABLE IF EXISTS cancion");
        db.execSQL(sqlCancion);
        db.execSQL(sqlAlbum);
    }
}
