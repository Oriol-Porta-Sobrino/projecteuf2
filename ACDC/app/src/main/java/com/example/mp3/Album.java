package com.example.mp3;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class Album extends AppCompatActivity {

    private MP3BBDD miMP3;
    private SQLiteDatabase db;
    private List<BBDDAlbum> lista;
    private ListaCanciones listar;
    Cursor cursor;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView titulo = (TextView) findViewById(R.id.txtTitulo);
        titulo.setText("Albums");

        lista = new ArrayList<BBDDAlbum>();

        cargarBase();
        llenarLista();

        listar = new ListaCanciones(this);

        ListView albums = (ListView) findViewById(R.id.listaCanciones);

        albums.setAdapter(listar);

        albums.setClickable(true);
        albums.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentLista = new Intent(getApplicationContext(), Lista.class);
                Bundle bundle = new Bundle();
                bundle.putInt("id", lista.get(position).getId());
                bundle.putString("nombre", lista.get(position).getNombre().toString());
                intentLista.putExtras(bundle);
                startActivity(intentLista);
            }
        });
    }

    public void cargarBase() {
        if (this.getDatabasePath("ACDC") != null) {
            miMP3 = new MP3BBDD(this, "ACDC", null, 1);
            SQLiteDatabase db = miMP3.getWritableDatabase();
            db.execSQL("INSERT INTO album (nombre)" +
                    " VALUES ('Highway to hell')");
            db.execSQL("INSERT INTO album (nombre)" +
                    " VALUES ('Back in black')");
            db.execSQL("INSERT INTO album (nombre)" +
                    " VALUES ('The Razors Edge')");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Beating around the bush', '" + R.raw.beating_around_the_bush + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Get it hot', '" + R.raw.get_it_hot + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Girls got rhythm', '" + R.raw.girls_got_rhythm + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Highway to hell', '" + R.raw.highway_to_hell + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('If you want blood you got it', '" + R.raw.if_you_want_blood_you_got_it + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Love hungry man', '" + R.raw.love_hungry_man + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Night prowler', '" + R.raw.night_prowler + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Shot down in flames', '" + R.raw.shot_down_in_flames + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Touch too much', '" + R.raw.touch_too_much + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Walk all over you', '" + R.raw.walk_all_over_you + "', 1)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Back in black', '" + R.raw.back_in_black + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Givin the dog a bone', '" + R.raw.givin_the_dog_a_bone + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Have a drink on me', '" + R.raw.have_a_drink_on_me + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Hells bells', '" + R.raw.hells_bells + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Let me put my love into you', '" + R.raw.let_me_put_my_love_into_you + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Rock and roll aint noise pollution', '" + R.raw.rock_and_roll_aint_noise_pollution + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Shake a leg', '" + R.raw.shake_a_leg + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Shoot to thrill', '" + R.raw.shoot_to_thrill + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('What do you do for money honey', '" + R.raw.what_do_you_do_for_money_honey + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('You shook me all night long', '" + R.raw.walk_all_over_you + "', 2)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Short of love', '" + R.raw.shot_of_love + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Are you ready', '" + R.raw.are_you_ready + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Fire your guns', '" + R.raw.fire_your_guns + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Goodbye and good riddance to bad luck', '" + R.raw.goodbye_and_good_riddance_to_bad_luck + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Got you by the balls', '" + R.raw.got_you_by_the_balls + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('If you dare', '" + R.raw.if_you_dare + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Lets make it', '" + R.raw.lets_make_it + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Mistress for christmas', '" + R.raw.mistress_for_christmas + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Moneytalk', '" + R.raw.moneytalk + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Rock your heart out', '" + R.raw.rock_your_heart_out + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('The razors edge', '" + R.raw.the_razors_edge + "', 3)");
            db.execSQL("INSERT INTO cancion (nombre, path, album)" +
                    " VALUES ('Thunderstruck', '" + R.raw.thunderstruck + "', 3)");
            db.close();
        }
    }

    public void llenarLista() {
        miMP3 = new MP3BBDD(this, "ACDC", null, 1);
        SQLiteDatabase db = miMP3.getWritableDatabase();
        BBDDAlbum objAlbum;
        cursor = db.rawQuery("SELECT id, nombre FROM album", null);
        if (cursor != null && cursor.moveToFirst())
        {
            do {
                int id = cursor.getInt(0);
                String nombre = cursor.getString(1);
                objAlbum = new BBDDAlbum(id, nombre);
                lista.add(objAlbum);
            } while (cursor.moveToNext());

        }
        db.close();
    }

    class ListaCanciones extends ArrayAdapter {

        Activity context;
        public ListaCanciones(Activity context) {
            super(context, R.layout.listar, lista);
            this.context = (Activity) context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listar, null);
            TextView txtNom = (TextView) item.findViewById(R.id.txtNombre);
            txtNom.setText(lista.get(position).getNombre().toString());
            return (item);
        }

    }

}
