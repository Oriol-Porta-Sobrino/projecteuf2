package com.example.mp3;

public class BBDDCancion {

    private int id;
    private String nombre;
    private int path;

    public BBDDCancion(int id, String nombre, int path) {
        this.id = id;
        this.nombre = nombre;
        this.path = path;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPath() {
        return path;
    }

    public void setPath(int path) {
        this.path = path;
    }
}
