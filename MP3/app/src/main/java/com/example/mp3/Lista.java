package com.example.mp3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Lista extends AppCompatActivity {

    private MP3BBDD miMP3;
    private List<String> lista;
    private ListaCanciones listar;
    private Cursor cursor;
    private Bundle bundleT;
    int cont = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lista = new ArrayList<String>();
        bundleT = this.getIntent().getExtras();
        llenarLista();
        listar = new ListaCanciones(this);

        ListView canciones = (ListView) findViewById(R.id.listaCanciones);

        canciones.setAdapter(listar);

        canciones.setClickable(true);
        canciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intentLuchador = new Intent(getApplicationContext(), Lista.class);
                Bundle bundle = new Bundle();
                bundle.putInt("position", position);
                bundle.putInt("id", bundleT.getInt("id"));
                bundle.putString("nombre", lista.get(position));
                intentLuchador.putExtras(bundle);
                startActivity(intentLuchador);
            }
        });

    }

    public void llenarLista() {

        String[] args = new String[] {bundleT.getInt("id")+""};
        miMP3 = new MP3BBDD(this, "ACDC", null, 1);
        SQLiteDatabase db = miMP3.getWritableDatabase();
        cursor = db.rawQuery("SELECT id, nombre, path FROM cancion WHERE album = ?", args);
        if (cursor!= null && cursor.moveToFirst())
        {
            do {
                String nombre = cursor.getString(1);
                lista.add(nombre);
            } while (cursor.moveToNext());

        }
        db.close();
    }

    /*public void cargarCanciones() {
        miMP3 = new MP3BBDD(this, "Cancion", null, 1);
        db = miMP3.getWritableDatabase();
        lista = new ArrayList<String>();
        File f = new File(Environment.getExternalStorageState()+ "/10D9-FE5B/Music");
        File[] files = f.listFiles();
        for (int i = 0; i < files.length; i++)
        {
            File file = files[i];
            lista.add(file.getName());
            db.execSQL("INSERT INTO cancion (nombre, ruta)" +
                    "VALUES ('" + file.getName().toString() + "', '" + file.getPath().toString() + "')");
            /*if (file.isDirectory())
                item.add(file.getName() + "/");
                //Si es fichero...
            else
                item.add(file.getName());*/
        /*}
        db.close();
    }

    public void cargarCanciones2() {
        ContentResolver musicResolver = getContentResolver();
        //Uri musicUri = Uri.parse("/Music/Musica/zoo");
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Log.i("MDM",musicUri.toString());
        //Cursor musicCursor = musicResolver.query(musicUri, projection, null, null, null);
        //Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null, null);
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE
        };
        Cursor musicCursor = musicResolver.query(musicUri, projection, null, null, null);

        Log.i("MDM","hi");
        if(musicCursor!=null && musicCursor.moveToFirst()){
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.ARTIST);
            //add songs to list
            do {
                Log.i("MDM","Passo" + musicCursor.getCount());
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                lista.add(thisTitle);
                db.execSQL("INSERT INTO cancion (nombre)" +
                        "VALUES ('" + thisTitle + "')");
            }
            while (musicCursor.moveToNext());
        }
    }*/

    class ListaCanciones extends ArrayAdapter {

        Activity context;
        public ListaCanciones(Activity context) {
            super(context, R.layout.listar, lista);
            this.context = (Activity) context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listar, null);
            TextView txtNom = (TextView) item.findViewById(R.id.txtNombre);
            txtNom.setText(lista.get(position));
            return (item);
        }

    }
}