package com.example.mp3;

public class BBDDAlbum {

    private int id;
    private String nombre;

    public BBDDAlbum(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
