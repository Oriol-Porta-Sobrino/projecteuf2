package com.example.mp3;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class Album extends AppCompatActivity {

    private MP3BBDD miMP3;
    private SQLiteDatabase db;
    private List<String> lista;
    private ListaCanciones listar;
    private List<Song> songs;
    Cursor cursor;
    private static final int MY_PERMISSION_REQUEST=1;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(ContextCompat.checkSelfPermission(Album.this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(Album.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)){
                ActivityCompat.requestPermissions(Album.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST);
            }else{
                ActivityCompat.requestPermissions(Album.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSION_REQUEST);
            }
        }else{

            //  mediaPlayer = new MediaPlayer();

        esto();



        }



    }

    public void esto() {
        TextView titulo = (TextView) findViewById(R.id.txtTitulo);
        titulo.setText("Albums");
        songs = new ArrayList<Song>();
        loadSongs();
        lista = new ArrayList<String>();

        listar = new ListaCanciones(this);

        ListView albums = (ListView) findViewById(R.id.listaCanciones);

        albums.setAdapter(listar);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String[] permissions,  int[] grantResults) {
        switch (requestCode){
            case MY_PERMISSION_REQUEST:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(Album.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED){
                        Toast.makeText(this,"Permiso Aceptado",Toast.LENGTH_SHORT).show();
                        esto();
                    }
                }else {
                    Toast.makeText(this,"Permiso Denegado",Toast.LENGTH_SHORT).show();
                    finish();

                }
                return;
            }
        }
    }

    private void loadSongs() {
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;  // Agafa tots els fitxer de musica
        String ismusic = MediaStore.Audio.Media.IS_MUSIC;
        Cursor cursor = this.getContentResolver().query(uri, null, ismusic, null, null);  // Els carrega a un cursor
        if (cursor != null) {
            while (cursor.moveToNext()) {   // Recorre el cursor amb tots els fitxer de musica i llegueis les metadates del cursor per estreure info
                String name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE));  // llegeix el títol de la cançó
                String author = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)); // llegeix l'artista
                String duration = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DURATION));  // Durada
                String durationFormat = setCorrectDuration(duration);
                String data = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA));
                songs.add(new Song(name, author, durationFormat, data));                         // Crea l'objecte Song i el fica a la llista que anirà al listview            }
            }


        }
    }

// Converteix la durada de milesegons a segons/minuts

        private String setCorrectDuration(String songs_duration) {
            if (Integer.valueOf(songs_duration) != null) {
                int time = Integer.valueOf(songs_duration);

                int seconds = time / 1000;
                int minutes = seconds / 60;
                seconds = seconds % 60;

                if (seconds < 10) {
                    songs_duration = String.valueOf(minutes) + ":0" + String.valueOf(seconds);
                } else {
                    songs_duration = String.valueOf(minutes) + ":" + String.valueOf(seconds);
                }
                return songs_duration;
            }
            return null;
        }

    public void cargarCanciones() {
        ContentResolver musicResolver = getContentResolver();
        //Uri musicUri = Uri.parse("/Music/Musica/zoo");
        Uri musicUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        Log.i("MDM",musicUri.toString());
        //Cursor musicCursor = musicResolver.query(musicUri, projection, null, null, null);
        //Cursor musicCursor = musicResolver.query(musicUri, null, null, null, null, null);
        String[] projection = {
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.TITLE
        };
        Cursor musicCursor = musicResolver.query(musicUri, projection, null, null, null);

        Log.i("MDM","hi");
        if(musicCursor!=null && musicCursor.moveToFirst()){
            //get columns
            int titleColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.TITLE);
            int idColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media._ID);
            int artistColumn = musicCursor.getColumnIndex
                    (MediaStore.Audio.Media.ARTIST);
            //add songs to list
            do {
                Log.i("MDM","Passo" + musicCursor.getCount());
                long thisId = musicCursor.getLong(idColumn);
                String thisTitle = musicCursor.getString(titleColumn);
                String thisArtist = musicCursor.getString(artistColumn);
                lista.add(thisTitle);
            }
            while (musicCursor.moveToNext());
        }
    }

    public class Song {
        private int idSong;
        private String nameSong;
        private String author;
        private String duration;
        private String uri;

        public Song(String nameSong, String author, String duration, String uri) {
            this.nameSong = nameSong;
            this.author = author;
            this.duration = duration;
            this.uri = uri;
        }
        public Song(int idSong,String nameSong, String author, String duration, String uri) {
            this.idSong = idSong;
            this.nameSong = nameSong;
            this.author = author;
            this.duration = duration;
            this.uri = uri;
        }
        public int getIdSong() {
            return idSong;
        }

        public String getAuthor() {
            return author;
        }

        public String getNameSong() {
            return nameSong;
        }

        public String getDuration() {
            return duration;
        }

        public String getUri() { return uri; }

        @Override
        public String toString() {
            return "Song{" +
                    "nameSong='" + nameSong + '\'' +
                    ", author='" + author + '\'' +
                    ", duration='" + duration + '\'' +
                    ", uri='" + uri + '\'' +
                    '}';
        }
    }

    class ListaCanciones extends ArrayAdapter {

        Activity context;
        public ListaCanciones(Activity context) {
            super(context, R.layout.listar, songs);
            this.context = (Activity) context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.listar, null);
            TextView txtNom = (TextView) item.findViewById(R.id.txtNombre);
            txtNom.setText(songs.get(position).getNameSong().toString());
            return (item);
        }

    }

}
